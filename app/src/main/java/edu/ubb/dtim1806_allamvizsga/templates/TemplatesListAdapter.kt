package edu.ubb.dtim1806_allamvizsga.templates

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import edu.ubb.dtim1806_allamvizsga.data.Template
import edu.ubb.dtim1806_allamvizsga.databinding.TemplatesListRowBinding

//Adapter class for listing templates via RecyclerView
class TemplatesListAdapter(val onItemClickListener: (item: Template) -> Unit) : ListAdapter<Template, TemplatesListAdapter.TemplateViewHolder>(diffUtil) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TemplateViewHolder {
        val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
        val viewHolder = TemplateViewHolder(TemplatesListRowBinding.inflate(layoutInflater, parent, false))
        viewHolder.binding.root.setOnClickListener { onItemClickListener(getItem(viewHolder.adapterPosition)) }
        return viewHolder
    }

    override fun onBindViewHolder(holder: TemplateViewHolder, position: Int) {
        val item: Template = getItem(position)
        val binding: TemplatesListRowBinding = holder.binding

        binding.templateName.text = item.name
        binding.templateAuthor.text = item.author

    }

    companion object {
        private val diffUtil: DiffUtil.ItemCallback<Template> = object : DiffUtil.ItemCallback<Template>() {
            override fun areItemsTheSame(oldItem: Template, newItem: Template): Boolean = (oldItem.name == newItem.name) && (oldItem.author == newItem.author)
            override fun areContentsTheSame(oldItem: Template, newItem: Template): Boolean = oldItem == newItem
        }
    }

    class TemplateViewHolder(val binding: TemplatesListRowBinding) : RecyclerView.ViewHolder(binding.root)

}