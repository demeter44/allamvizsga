package edu.ubb.dtim1806_allamvizsga.templates

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.fragment.app.Fragment
import com.google.android.material.textfield.TextInputEditText
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import edu.ubb.dtim1806_allamvizsga.R
import edu.ubb.dtim1806_allamvizsga.data.IntegerField
import edu.ubb.dtim1806_allamvizsga.data.Page
import edu.ubb.dtim1806_allamvizsga.data.StringField
import edu.ubb.dtim1806_allamvizsga.data.Template
import kotlinx.android.synthetic.main.templates_new.*
import java.io.File

//Creating a new template from scratch
class TemplatesNewFragment : Fragment() {
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val templatesDir = File(requireContext().filesDir, "templates")
        templatesDir.mkdir()
        return inflater.inflate(R.layout.templates_new, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        templatesNewTemplateBut.setOnClickListener {
            //We group all the views
            val stringFieldsGen: List<TextInputEditText> = listOf(stringFieldGen1, stringFieldGen2, stringFieldGen3, stringFieldGen4, stringFieldGen5, stringFieldGen6)
            val intFieldsGen: List<TextInputEditText> = listOf(intFieldGen1, intFieldGen2, intFieldGen3, intFieldGen4, intFieldGen5, intFieldGen6, intFieldGen7, intFieldGen8)
            val modChecksGen: List<AppCompatCheckBox> = listOf(intFieldGen1Mod, intFieldGen2Mod, intFieldGen3Mod, intFieldGen4Mod, intFieldGen5Mod, intFieldGen6Mod, intFieldGen7Mod, intFieldGen8Mod)
            val maxChecksGen: List<AppCompatCheckBox> = listOf(intFieldGen1Max, intFieldGen2Max, intFieldGen3Max, intFieldGen4Max, intFieldGen5Max, intFieldGen6Max, intFieldGen7Max, intFieldGen8Max)

            //generalStrings
            val stringStatsGeneral: MutableList<StringField> = mutableListOf()
            for (sf in stringFieldsGen) {
                if (sf.text.toString() != "") {
                    val temp = StringField(sf.text.toString(), "")
                    stringStatsGeneral.add(temp)
                }
            }

            //generalIntegers
            val intStatsGeneral: MutableList<IntegerField> = mutableListOf()
            for (i in 0..7) {
                if (intFieldsGen[i].text.toString() != "") {
                    val temp = IntegerField(intFieldsGen[i].text.toString(), 0)
                    if (modChecksGen[i].isChecked) {
                        temp.modifier = 0
                    }
                    if (maxChecksGen[i].isChecked) {
                        temp.maxValue = 0
                    }
                    intStatsGeneral.add(temp)
                }
            }

            //stats
            val intFieldsStat: List<TextInputEditText> = listOf(intFieldStat1, intFieldStat2, intFieldStat3, intFieldStat4, intFieldStat5, intFieldStat6, intFieldStat7, intFieldStat8)
            val modChecksStat: List<AppCompatCheckBox> = listOf(intFieldStat1Mod, intFieldStat2Mod, intFieldStat3Mod, intFieldStat4Mod, intFieldStat5Mod, intFieldStat6Mod, intFieldStat7Mod, intFieldStat8Mod)

            //statsI
            val intStatsStats: MutableList<IntegerField> = mutableListOf()
            for (i in 0..7) {
                if (intFieldsStat[i].text.toString() != "") {
                    val temp = IntegerField(intFieldsStat[i].text.toString(), 0)
                    if (modChecksStat[i].isChecked) {
                        temp.modifier = 0
                    }
                    intStatsStats.add(temp)
                }
            }

            //skills
            val intFieldsSkill: List<TextInputEditText> = listOf(intFieldSkill1, intFieldSkill2, intFieldSkill3, intFieldSkill4, intFieldSkill5, intFieldSkill6, intFieldSkill7, intFieldSkill8, intFieldSkill9, intFieldSkill10, intFieldSkill11, intFieldSkill12, intFieldSkill13, intFieldSkill14, intFieldSkill15, intFieldSkill16, intFieldSkill17, intFieldSkill18, intFieldSkill19, intFieldSkill20)
            val modChecksSkill: List<AppCompatCheckBox> = listOf(intFieldSkill1Mod, intFieldSkill2Mod, intFieldSkill3Mod, intFieldSkill4Mod, intFieldSkill5Mod, intFieldSkill6Mod, intFieldSkill7Mod, intFieldSkill8Mod, intFieldSkill9Mod, intFieldSkill10Mod, intFieldSkill11Mod, intFieldSkill12Mod, intFieldSkill13Mod, intFieldSkill14Mod, intFieldSkill15Mod, intFieldSkill16Mod, intFieldSkill17Mod, intFieldSkill18Mod, intFieldSkill19Mod, intFieldSkill20Mod)

            //skillsI
            val intStatsSkill: MutableList<IntegerField> = mutableListOf()
            for (i in 0..19) {
                if (intFieldsSkill[i].text.toString() != "") {
                    val temp = IntegerField(intFieldsSkill[i].text.toString(), 0)
                    if (modChecksSkill[i].isChecked) {
                        temp.modifier = 0
                    }
                    intStatsSkill.add(temp)
                }
            }

            val generalPage = Page("General:", intStatsGeneral, stringStatsGeneral)
            val statsPage = Page("Stats:", intStatsStats)
            val skillsPage = Page("Skills:", intStatsSkill)

            val template = Template(templatesNewText.text.toString(), getString(R.string.current_author), listOf(generalPage, statsPage, skillsPage))

            val moshi: Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
            val adapter: JsonAdapter<Template> = moshi.adapter(Template::class.java)

            val templateJson = adapter.toJson(template)

            //Save template as JSON on local file system
            val fileName = templatesNewText.text.toString() + ".json"
            val file = File(requireContext().filesDir.path + "/templates", fileName)
            file.writeText(templateJson)
            file.createNewFile()

            Toast.makeText(context, "Template created!", Toast.LENGTH_SHORT).show()

        }

    }
}