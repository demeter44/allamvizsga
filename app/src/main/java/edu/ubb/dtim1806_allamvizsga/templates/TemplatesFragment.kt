package edu.ubb.dtim1806_allamvizsga.templates

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import edu.ubb.dtim1806_allamvizsga.R
import edu.ubb.dtim1806_allamvizsga.data.Template
import kotlinx.android.synthetic.main.templates.*
import java.io.File

class TemplatesFragment : Fragment() {
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.templates, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        templatesNewBut.setOnClickListener { findNavController().navigate(TemplatesFragmentDirections.navigateToTemplatesNew()) }
        templatesBrowseBut.setOnClickListener { findNavController().navigate(TemplatesFragmentDirections.navigateToDbBrowse()) }

        val moshi: Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
        val adapter: JsonAdapter<Template> = moshi.adapter(Template::class.java)
        val templatesCollection: MutableList<Template> = mutableListOf()

        //Loading templates from local file system
        val templatesFilesDir = requireContext().filesDir.path + "/templates"
        val templates = File(templatesFilesDir)
        val files: Array<File>? = templates.listFiles()
        var templatesText = ""
        if (files != null) {
            for (file in files) {
                if (file.name.split(".")[1] == "json") {
                    templatesText += file.name
                    val temp = adapter.fromJson(file.readText())
                    if (temp != null) {
                        templatesCollection.add(temp)
                    }
                }
            }
        }

        val listAdapter = TemplatesListAdapter { item -> onItemClick(item) }
        templateRecycler.adapter = listAdapter
        templateRecycler.layoutManager = LinearLayoutManager(context)
        listAdapter.submitList(templatesCollection)


        templatesList.text = templatesText


    }

    private fun onItemClick(item: Template) {
        println(item.pages)
    }

}