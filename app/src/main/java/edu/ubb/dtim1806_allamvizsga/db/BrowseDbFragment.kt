package edu.ubb.dtim1806_allamvizsga.db

import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import edu.ubb.dtim1806_allamvizsga.R
import edu.ubb.dtim1806_allamvizsga.data.Template
import edu.ubb.dtim1806_allamvizsga.templates.TemplatesListAdapter
import io.realm.Realm
import io.realm.RealmChangeListener
import io.realm.kotlin.where
import io.realm.mongodb.App
import io.realm.mongodb.AppConfiguration
import io.realm.mongodb.Credentials
import io.realm.mongodb.sync.SyncConfiguration
import kotlinx.android.synthetic.main.db_browse.*
import java.io.File

class BrowseDbFragment : Fragment() {

    private lateinit var app: App

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.db_browse, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //We initialize everything needed for the Realms API
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        app = App(AppConfiguration.Builder(getString(R.string.app_id)).build())
        val credentials: Credentials = Credentials.anonymous()
        val templatesCollection: MutableList<Template> = mutableListOf()
        val listAdapter = TemplatesListAdapter { item -> onItemClick(item) }
        templateRecyclerDb.adapter = listAdapter
        templateRecyclerDb.layoutManager = LinearLayoutManager(context)
        listAdapter.submitList(templatesCollection)

        //Anonymous login
        app.loginAsync(credentials) {
            if (it.isSuccess) {
                Log.e("QUICKSTART", "Successfully authenticated anonymously.")
                val partitionValue = "testauthor"
                val config = SyncConfiguration.Builder(app.currentUser(), partitionValue)
                        .allowQueriesOnUiThread(true)
                        .allowWritesOnUiThread(true)
                        .build()
                val uiThreadRealm = Realm.getInstance(config)

////                  Insert a document
//                val testintf = IntegerField("intf",12)
//                val teststrf = StringField("strf","test")
//                val testpage = Page("testpage",listOf(testintf), listOf(teststrf))
//                val testtemplate = edu.ubb.dtim1806_allamvizsga.data.Template("testname",partitionValue,
//                    listOf(testpage))
//                val testmodel = dataToModel(testtemplate)
//
//                uiThreadRealm.executeTransaction {
//                    transactionRealm ->
//                    transactionRealm.insert(testmodel)
//                }

                //Async query to get all templates from Realms
                val templates = uiThreadRealm.where<edu.ubb.dtim1806_allamvizsga.db.Template>().findAllAsync()
                templates.addChangeListener(RealmChangeListener {
                    println("query completed")
                    println("TEMPLATES FROM DB")
                    println(templates.forEach { t ->
                        println(modelToData(t))
                        templatesCollection.add(0, modelToData(t))
                        listAdapter.notifyItemInserted(0)
                    })
                })

            } else {
                Log.e("QUICKSTART", "Failed to log in. Error: ${it.error}")
            }
        }
    }

    //We assure a safe logout
    override fun onDestroy() {
        app.currentUser()?.logOutAsync {
            if (it.isSuccess) {
                println("successful db logout")
            } else {
                println("error logging out of db: ${it.error}")
            }
        }
        super.onDestroy()
    }

    //Clicking a template downloads it
    private fun onItemClick(item: Template) {

        val moshi: Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
        val adapter: JsonAdapter<Template> = moshi.adapter(Template::class.java)

        val templateJson = adapter.toJson(item)

        val fileName = item.name + ".json"
        val file = File(requireContext().filesDir.path + "/templates", fileName)
        file.writeText(templateJson)
        file.createNewFile()
        Toast.makeText(context, "Template downloaded!", Toast.LENGTH_LONG).show()


    }

}