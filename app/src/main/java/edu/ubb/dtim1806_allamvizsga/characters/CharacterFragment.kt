package edu.ubb.dtim1806_allamvizsga.characters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import edu.ubb.dtim1806_allamvizsga.R
import edu.ubb.dtim1806_allamvizsga.data.CharacterPage
import kotlinx.android.synthetic.main.characters.*
import java.io.File

class CharacterFragment : Fragment() {
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.characters, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        characterNewBut.setOnClickListener { findNavController().navigate(CharacterFragmentDirections.navigateToCharactersNew()) }

        val moshi: Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
        val adapter: JsonAdapter<CharacterPage> = moshi.adapter(CharacterPage::class.java)
        val charactersCollection: MutableList<CharacterPage> = mutableListOf()

        //Load characters from local file system
        val charactersFilesDir = requireContext().filesDir.path + "/characters"
        val characters = File(charactersFilesDir)
        val files: Array<File>? = characters.listFiles()
        if (files != null) {
            for (file in files) {
                if (file.name.split(".")[1] == "json") {
                    val temp = adapter.fromJson(file.readText())
                    if (temp != null) {
                        charactersCollection.add(temp)
                    }
                }
            }
        }

        val listAdapter = CharactersListAdapter { item -> onItemClick(item) }
        charactersRecyclerView.adapter = listAdapter
        charactersRecyclerView.layoutManager = LinearLayoutManager(context)
        listAdapter.submitList(charactersCollection)

    }

    private fun onItemClick(c: CharacterPage) {
        println(c)
    }
}