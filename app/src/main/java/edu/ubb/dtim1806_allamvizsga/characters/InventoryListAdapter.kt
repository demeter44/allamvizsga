package edu.ubb.dtim1806_allamvizsga.characters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import edu.ubb.dtim1806_allamvizsga.data.CharacterItem
import edu.ubb.dtim1806_allamvizsga.databinding.InventoryListRowBinding

//Adapter to display inventory items in a RecyclerView
class InventoryListAdapter(val onItemClickListener: (item: CharacterItem) -> Unit) : ListAdapter<CharacterItem, InventoryListAdapter.CharacterItemViewHolder>(diffUtil) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterItemViewHolder {
        val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
        return CharacterItemViewHolder(InventoryListRowBinding.inflate(layoutInflater, parent, false))
    }

    override fun onBindViewHolder(holder: CharacterItemViewHolder, position: Int) {
        val item: CharacterItem = getItem(position)
        val binding: InventoryListRowBinding = holder.binding

        binding.itemNameList.text = item.name
        binding.itemDescriptionList.text = item.description
        binding.itemQuantityList.text = item.quantity.toString()
        binding.giveItemBut.setOnClickListener { onItemClickListener(getItem(position)) }

    }

    companion object {
        private val diffUtil: DiffUtil.ItemCallback<CharacterItem> = object : DiffUtil.ItemCallback<CharacterItem>() {
            override fun areItemsTheSame(oldItem: CharacterItem, newItem: CharacterItem): Boolean = (oldItem.name == newItem.name) && (oldItem.description == newItem.description)
            override fun areContentsTheSame(oldItem: CharacterItem, newItem: CharacterItem): Boolean = oldItem == newItem
        }
    }

    class CharacterItemViewHolder(val binding: InventoryListRowBinding) : RecyclerView.ViewHolder(binding.root)

}