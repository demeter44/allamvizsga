package edu.ubb.dtim1806_allamvizsga.characters

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import edu.ubb.dtim1806_allamvizsga.R
import edu.ubb.dtim1806_allamvizsga.data.Template
import edu.ubb.dtim1806_allamvizsga.templates.TemplatesListAdapter
import kotlinx.android.synthetic.main.characters_new.*
import java.io.File

class CharacterNewFragment : Fragment() {
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val templatesDir = File(requireContext().filesDir, "templates")
        templatesDir.mkdir()
        return inflater.inflate(R.layout.characters_new, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val moshi: Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
        val adapter: JsonAdapter<Template> = moshi.adapter(Template::class.java)
        val templatesCollection: MutableList<Template> = mutableListOf()

        //Load available templates
        val templatesFilesDir = requireContext().filesDir.path + "/templates"
        val templates = File(templatesFilesDir)
        val files: Array<File>? = templates.listFiles()
        var templatesText = ""
        if (files != null) {
            for (file in files) {
                if (file.name.split(".")[1] == "json") {
                    templatesText += file.name
                    val temp = adapter.fromJson(file.readText())
                    if (temp != null) {
                        templatesCollection.add(temp)
                    }
                }
            }
        }

        val listAdapter = TemplatesListAdapter { item -> onItemClick(item) }
        templateRecyclerChar.adapter = listAdapter
        templateRecyclerChar.layoutManager = LinearLayoutManager(context)
        listAdapter.submitList(templatesCollection)
    }

    private fun onItemClick(item: Template) {
        findNavController().navigate(CharacterNewFragmentDirections.navigateToCharactersCreation(item))
    }
}