package edu.ubb.dtim1806_allamvizsga.characters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import edu.ubb.dtim1806_allamvizsga.data.CharacterPage
import edu.ubb.dtim1806_allamvizsga.databinding.CharactersListRowBinding

//Adapter to display characters in a RecyclerView
class CharactersListAdapter(val onItemClickListener: (item: CharacterPage) -> Unit) : ListAdapter<CharacterPage, CharactersListAdapter.CharacterViewHolder>(diffUtil) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {
        val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
        val viewHolder = CharacterViewHolder(CharactersListRowBinding.inflate(layoutInflater, parent, false))
        viewHolder.binding.root.setOnClickListener { onItemClickListener(getItem(viewHolder.adapterPosition)) }
        return viewHolder
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        val item: CharacterPage = getItem(position)
        val binding: CharactersListRowBinding = holder.binding

        binding.characterName.text = item.name
        binding.characterTemplate.text = item.template.name

    }

    companion object {
        private val diffUtil: DiffUtil.ItemCallback<CharacterPage> = object : DiffUtil.ItemCallback<CharacterPage>() {
            override fun areItemsTheSame(oldItem: CharacterPage, newItem: CharacterPage): Boolean = (oldItem.name == newItem.name) && (oldItem.template == newItem.template)
            override fun areContentsTheSame(oldItem: CharacterPage, newItem: CharacterPage): Boolean = oldItem == newItem
        }
    }

    class CharacterViewHolder(val binding: CharactersListRowBinding) : RecyclerView.ViewHolder(binding.root)

}