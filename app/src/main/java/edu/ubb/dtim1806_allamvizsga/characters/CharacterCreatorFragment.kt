package edu.ubb.dtim1806_allamvizsga.characters

import android.graphics.Color
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import edu.ubb.dtim1806_allamvizsga.R
import edu.ubb.dtim1806_allamvizsga.data.CharacterPage
import kotlinx.android.synthetic.main.characters_creation.*
import java.io.File

class CharacterCreatorFragment : Fragment() {
    private val args: CharacterCreatorFragmentArgs by navArgs()

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.characters_creation, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //We create the UI programmatically so it is fully dynamic
        val stringFieldsMap = mutableMapOf<String, EditText>()
        val intFieldsMap = mutableMapOf<String, EditText>()
        for (i in args.template.pages) {
            val blockname = AppCompatTextView(requireContext())
            blockname.width = 200
            blockname.height = 200
            blockname.text = i.name
            blockname.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
            blockname.textSize = 22F
            parentLinearLayout.addView(blockname)

            if (i.stringFields != null) {
                for (k in i.stringFields) {
                    val inner = LinearLayoutCompat(requireContext())
                    inner.orientation = LinearLayoutCompat.HORIZONTAL
                    val n1 = AppCompatTextView(requireContext())
                    n1.text = k.name
                    n1.width = 180
                    n1.height = 150
                    n1.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
                    n1.textSize = 16F
                    val t1 = EditText(requireContext())
                    t1.hint = k.value
                    t1.width = 600
                    t1.height = 150
                    inner.addView(n1)
                    inner.addView(t1)
                    parentLinearLayout.addView(inner)
                    stringFieldsMap[k.name] = t1
                }
            }

            if (i.intFields != null) {
                for (j in i.intFields) {
                    val inner = LinearLayoutCompat(requireContext())
                    inner.orientation = LinearLayoutCompat.HORIZONTAL
                    val n1 = AppCompatTextView(requireContext())
                    n1.text = j.name
                    n1.width = 150
                    n1.height = 150
                    n1.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
                    n1.textSize = 16F
                    val t1 = EditText(requireContext())
                    t1.inputType = (InputType.TYPE_NUMBER_FLAG_SIGNED or InputType.TYPE_CLASS_NUMBER)
                    t1.hint = j.value.toString()
                    t1.width = 200
                    t1.height = 150
                    inner.addView(n1)
                    inner.addView(t1)
                    parentLinearLayout.addView(inner)
                    intFieldsMap[j.name] = t1
                }
            }
            val sep = View(requireContext())
            sep.minimumWidth = 10
            sep.minimumHeight = 10
            sep.setBackgroundColor(Color.parseColor("#000000"))
            parentLinearLayout.addView(sep)
        }

        val saveButton = AppCompatButton(requireContext())
        saveButton.text = getString(R.string.save)
        saveButton.width = 100
        saveButton.height = 100

        //Save the character
        saveButton.setOnClickListener {
            try {
                for (i in args.template.pages) {
                    if (i.stringFields != null) {
                        for (k in i.stringFields) {
                            k.value = stringFieldsMap[k.name]?.text.toString()
                        }
                    }

                    if (i.intFields != null) {
                        for (j in i.intFields) {
                            j.value = intFieldsMap[j.name]?.text.toString().toInt()
                        }
                    }

                }

                val character = CharacterPage(characterCreationName.text.toString(), args.template, mutableListOf())

                val moshi: Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
                val adapter: JsonAdapter<CharacterPage> = moshi.adapter(CharacterPage::class.java)

                val characterJson = adapter.toJson(character)

                val fileName = characterCreationName.text.toString() + "_" + args.template.name + ".json"

                val file = File(requireContext().filesDir.path + "/characters", fileName)
                val cdir = File(requireContext().filesDir.path + "/characters")
                cdir.mkdir()
                file.createNewFile()
                file.writeText(characterJson)
                println(characterJson)
                Toast.makeText(requireContext(), "Character created!", Toast.LENGTH_SHORT).show()
            } catch (e: NumberFormatException) {
                Toast.makeText(requireContext(), "Invalid Argument", Toast.LENGTH_SHORT).show()
            }
        }

        parentLinearLayout.addView(saveButton)


    }
}
