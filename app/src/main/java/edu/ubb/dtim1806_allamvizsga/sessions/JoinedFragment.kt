package edu.ubb.dtim1806_allamvizsga.sessions

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.PopupWindow
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat.getColor
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.nearby.Nearby
import com.google.android.gms.nearby.connection.*
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import edu.ubb.dtim1806_allamvizsga.R
import edu.ubb.dtim1806_allamvizsga.characters.InventoryListAdapter
import edu.ubb.dtim1806_allamvizsga.data.CharacterItem
import edu.ubb.dtim1806_allamvizsga.data.CharacterPage
import kotlinx.android.synthetic.main.joined_session.*
import java.io.File

class JoinedFragment : Fragment() {

    private val args: JoinedFragmentArgs by navArgs()

    private val playerList: MutableList<String> = mutableListOf()
    private val playerNameList: MutableList<String> = mutableListOf()
    private val idToPlayer: MutableMap<String, String> = mutableMapOf()
    private val playerToId: MutableMap<String, String> = mutableMapOf()

    private lateinit var playerListAdapter: ConnectionsListAdapter

    private val inventoryAdapter = InventoryListAdapter { item -> onGiveClicked(item) }


    //Callback that handles receiving payloads
    private val payloadCallback = object : PayloadCallback() {
        override fun onPayloadReceived(endpointId: String, payload: Payload) {
            val tempString = String(payload.asBytes()!!)
            Log.i("tag", "Payload received")
            Log.i("tag", "Payload is $tempString")
            when (tempString.split(" ")[0]) {
                "SENDPLAYERS" -> {
                    println("Received response to sendplayers")
                    println(tempString)
                    val playerData = tempString.split(" ")
                    for (i in 1 until playerData.size - 1) {
                        println(playerData[i])
                        val playerId = playerData[i].split('%')[0]
                        val playerName = playerData[i].split('%')[1]
                        playerList.add(0, playerId)
                        playerNameList.add(0, playerName)
                        idToPlayer[playerId] = playerName
                        playerToId[playerName] = playerId
                        playerListAdapter.notifyItemInserted(0)
                    }
                }

                "FORWARDITEM" -> {
                    println("Received item")
                    val itemData = tempString.split(" ")
                    val itemName = itemData[1]
                    val itemDescription = itemData[2]
                    val itemQuantity = itemData[3]
                    val receivedFrom = itemData[4]
                    val temp = CharacterItem(itemName, itemDescription, itemQuantity.toInt())
                    args.characterPage.inventory.add(0, temp)
                    inventoryAdapter.notifyItemInserted(0)
                    Toast.makeText(context, "Received item from $receivedFrom!", Toast.LENGTH_SHORT).show()
                }

            }
        }

        override fun onPayloadTransferUpdate(endpointId: String, update: PayloadTransferUpdate) {
            Log.i("tag", "Payload transfer update")
        }

    }

    //Callback that handles connections
    private val connectionLifecycleCallback = object : ConnectionLifecycleCallback() {
        override fun onConnectionInitiated(endpointId: String, connectionInfo: ConnectionInfo) {
            Log.i("tag", "Accepting connection")
            Nearby.getConnectionsClient(requireContext())
                    .acceptConnection(endpointId, payloadCallback)

        }

        override fun onConnectionResult(endpointId: String, result: ConnectionResolution) {
            if (result.status.isSuccess) {
                Log.i("tag", "Connection successful")
                Nearby.getConnectionsClient(requireContext()).stopDiscovery() //change this later
            } else {
                Log.i("tag", "Connection failed")
            }
        }

        override fun onDisconnected(endpointId: String) {
            Log.i("tag", "Disconnected")
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.joined_session, container, false)
    }

    @SuppressLint("InflateParams")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Nearby.getConnectionsClient(requireContext()).requestConnection(args.connectionName, args.connectionId, connectionLifecycleCallback)

        //We create the UI dynamically
        val stringFieldsMap = mutableMapOf<String, EditText>()
        val intFieldsMap = mutableMapOf<String, EditText>()
        var oldText = ""
        characterNameJoined.text = args.characterPage.name
        for (i in args.characterPage.template.pages) {
            val blockname = AppCompatTextView(requireContext())
            blockname.width = 200
            blockname.height = 200
            blockname.text = i.name
            blockname.setTextColor(getColor(requireContext(), R.color.white))
            blockname.textSize = 22F
            parentJoinedLinearLayout.addView(blockname)

            if (i.stringFields != null) {
                for (k in i.stringFields) {
                    val inner = LinearLayoutCompat(requireContext())
                    inner.orientation = LinearLayoutCompat.HORIZONTAL
                    val n1 = AppCompatTextView(requireContext())
                    n1.text = k.name
                    n1.width = 180
                    n1.height = 150
                    n1.setTextColor(getColor(requireContext(), R.color.white))
                    n1.textSize = 16F
                    val t1 = EditText(requireContext())
                    if (k.value != "") t1.setText(k.value)
                    t1.hint = k.value
                    t1.width = 600
                    t1.height = 150

                    t1.setOnFocusChangeListener { _, hasFocus ->
                        if (hasFocus) {
                            oldText = t1.text.toString()
                        } else {
                            if (oldText != t1.text.toString()) {
                                k.value = t1.text.toString()
                                println("sending payload")
                                val bytesPayload = Payload.fromBytes("Value of ${n1.text} changed to ${t1.text}. Previous value: $oldText".toByteArray(Charsets.UTF_8))
                                Nearby.getConnectionsClient(requireContext()).sendPayload(args.connectionId, bytesPayload)
                            }
                        }
                    }

                    inner.addView(n1)
                    inner.addView(t1)
                    parentJoinedLinearLayout.addView(inner)
                    stringFieldsMap[k.name] = t1
                }
            }

            if (i.intFields != null) {
                for (j in i.intFields) {
                    val inner = LinearLayoutCompat(requireContext())
                    inner.orientation = LinearLayoutCompat.HORIZONTAL
                    val n1 = AppCompatTextView(requireContext())
                    n1.text = j.name
                    n1.width = 150
                    n1.height = 150
                    n1.setTextColor(getColor(requireContext(), R.color.white))
                    n1.textSize = 16F
                    val t1 = EditText(requireContext())
                    t1.inputType = (InputType.TYPE_NUMBER_FLAG_SIGNED or InputType.TYPE_CLASS_NUMBER)
                    if (j.value != 0) t1.setText(j.value.toString())
                    t1.hint = j.value.toString()
                    t1.width = 200
                    t1.height = 150

                    t1.setOnFocusChangeListener { _, hasFocus ->
                        if (hasFocus) {
                            oldText = t1.text.toString()
                        } else {
                            if (oldText != t1.text.toString()) {
                                try {
                                    j.value = t1.text.toString().toInt()
                                } catch (e: NumberFormatException) {
                                    println("illegal argument")
                                }
                                println("sending payload")
                                val bytesPayload = Payload.fromBytes("Value of ${n1.text} changed to ${t1.text}".toByteArray(Charsets.UTF_8))
                                Nearby.getConnectionsClient(requireContext()).sendPayload(args.connectionId, bytesPayload)
                            }
                        }
                    }

                    inner.addView(n1)
                    inner.addView(t1)
                    parentJoinedLinearLayout.addView(inner)
                    intFieldsMap[j.name] = t1
                }
            }

            val sep = View(requireContext())
            sep.minimumWidth = 10
            sep.minimumHeight = 10
            sep.setBackgroundColor(Color.parseColor("#000000"))
            parentJoinedLinearLayout.addView(sep)

        }


        //Creating popup menu for inventory management
        val popupViewInventory = LayoutInflater.from(requireContext()).inflate(R.layout.inventory_window, null)

        val popupWindowInventory = PopupWindow(popupViewInventory, ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT, true)
        manageInventoryBut.setOnClickListener { popupWindowInventory.showAtLocation(view, Gravity.CENTER, 0, 0) }

        val inventoryRecycler = popupViewInventory.findViewById<RecyclerView>(R.id.itemList)
        inventoryRecycler.adapter = inventoryAdapter
        inventoryRecycler.layoutManager = LinearLayoutManager(context)
        inventoryAdapter.submitList(args.characterPage.inventory)

        val popupViewNewItem = LayoutInflater.from(requireContext()).inflate(R.layout.add_item_window, null)

        val popupWindowNewItem = PopupWindow(popupViewNewItem, ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT, true)


        val addItemBut = popupViewInventory.findViewById<AppCompatButton>(R.id.addItemBut)
        addItemBut.setOnClickListener { popupWindowNewItem.showAtLocation(view, Gravity.CENTER, 0, 0) }

        val newItemName = popupViewNewItem.findViewById<AppCompatEditText>(R.id.itemName)
        val newItemQuantity = popupViewNewItem.findViewById<AppCompatEditText>(R.id.itemQuantity)
        newItemQuantity.inputType = (InputType.TYPE_NUMBER_FLAG_SIGNED or InputType.TYPE_CLASS_NUMBER)
        val newItemDescription = popupViewNewItem.findViewById<AppCompatEditText>(R.id.itemDescription)
        val addNewItemBut = popupViewNewItem.findViewById<AppCompatButton>(R.id.addItemFinalBut)
        addNewItemBut.setOnClickListener {
            val newItem = CharacterItem(newItemName.text.toString(), newItemDescription.text.toString(), newItemQuantity.text.toString().toInt())
            args.characterPage.inventory.add(newItem)
            println("new item added: ${newItem.name}")
            val bytesPayload = Payload.fromBytes("Added new item: ${newItem.name} with quantity: ${newItem.quantity}".toByteArray(Charsets.UTF_8))
            Nearby.getConnectionsClient(requireContext()).sendPayload(args.connectionId, bytesPayload)
            popupWindowNewItem.dismiss()
        }

    }

    //Give item menu
    @SuppressLint("InflateParams")
    fun onGiveClicked(item: CharacterItem) {
        playerNameList.clear()
        playerList.clear()
        playerToId.clear()
        idToPlayer.clear()
        val popupPlayerList = LayoutInflater.from(requireContext()).inflate(R.layout.give_window, null)
        val popupPlayerWindow = PopupWindow(popupPlayerList, ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT, true)
        popupPlayerWindow.showAtLocation(view, Gravity.CENTER, 0, 0)

        playerListAdapter = ConnectionsListAdapter { player -> playerToId[player]?.let { onPlayerClick(it, item, popupPlayerWindow) } }
        val playerRecycler = popupPlayerList.findViewById<RecyclerView>(R.id.playersRecycler)

        playerRecycler.adapter = playerListAdapter
        playerRecycler.layoutManager = LinearLayoutManager(context)
        playerListAdapter.submitList(playerNameList)

        println("Sending payload to request players")
        val bytesPayload = Payload.fromBytes("GETPLAYERS".toByteArray(Charsets.UTF_8))
        Nearby.getConnectionsClient(requireContext()).sendPayload(args.connectionId, bytesPayload)
    }

    //Send the item to the specified player
    private fun onPlayerClick(playerId: String, item: CharacterItem, window: PopupWindow) {
        println("Player clicked: $playerId with item ${item.name}")
        println("Sending payload to send item")
        val bytesPayload = Payload.fromBytes("SENDITEM ${item.name} ${item.description} ${item.quantity} $playerId".toByteArray(Charsets.UTF_8))
        Nearby.getConnectionsClient(requireContext()).sendPayload(args.connectionId, bytesPayload)
        val index = args.characterPage.inventory.indexOf(item)
        args.characterPage.inventory.removeAt(index)
        inventoryAdapter.notifyItemRemoved(index)
        window.dismiss()
        playerNameList.clear()
        playerList.clear()
        playerToId.clear()
        idToPlayer.clear()
    }

    //Always save the character on fragment pause
    override fun onStop() {
        println("Stop begin")

        val moshi: Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
        val adapter: JsonAdapter<CharacterPage> = moshi.adapter(CharacterPage::class.java)

        val characterJson = adapter.toJson(args.characterPage)

        val fileName = args.characterPage.name + "_" + args.characterPage.template.name + ".json"

        val file = File(requireContext().filesDir.path + "/characters", fileName)
        val cdir = File(requireContext().filesDir.path + "/characters")
        cdir.mkdir()
        file.createNewFile()
        file.writeText(characterJson)
        println(characterJson)

        println("Stop ended")
        super.onStop()
    }


}