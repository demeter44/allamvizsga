package edu.ubb.dtim1806_allamvizsga.sessions

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import edu.ubb.dtim1806_allamvizsga.databinding.ConnectionsListRowBinding

//Adapter to display connections in a RecyclerView
class ConnectionsListAdapter(val onItemClickListener: (item: String) -> Unit) : ListAdapter<String, ConnectionsListAdapter.ConnectionsViewHolder>(diffUtil) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConnectionsViewHolder {
        val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
        val viewHolder = ConnectionsViewHolder(ConnectionsListRowBinding.inflate(layoutInflater, parent, false))
        viewHolder.binding.root.setOnClickListener { onItemClickListener(getItem(viewHolder.adapterPosition)) }
        return viewHolder
    }

    override fun onBindViewHolder(holder: ConnectionsViewHolder, position: Int) {
        val item: String = getItem(position)
        val binding: ConnectionsListRowBinding = holder.binding

        val data = item.split('%')
        if (data.size > 1) {
            binding.connectionId.text = data[1]
        } else {
            binding.connectionId.text = item
        }

    }

    companion object {
        private val diffUtil: DiffUtil.ItemCallback<String> = object : DiffUtil.ItemCallback<String>() {
            override fun areItemsTheSame(oldItem: String, newItem: String): Boolean = oldItem == newItem
            override fun areContentsTheSame(oldItem: String, newItem: String): Boolean = oldItem == newItem
        }
    }

    class ConnectionsViewHolder(val binding: ConnectionsListRowBinding) : RecyclerView.ViewHolder(binding.root)

}