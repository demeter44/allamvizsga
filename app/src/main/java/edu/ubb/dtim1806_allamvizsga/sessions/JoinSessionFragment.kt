package edu.ubb.dtim1806_allamvizsga.sessions

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.nearby.Nearby
import com.google.android.gms.nearby.connection.DiscoveredEndpointInfo
import com.google.android.gms.nearby.connection.DiscoveryOptions
import com.google.android.gms.nearby.connection.EndpointDiscoveryCallback
import com.google.android.gms.nearby.connection.Strategy
import edu.ubb.dtim1806_allamvizsga.R
import kotlinx.android.synthetic.main.join_session.*


class JoinSessionFragment : Fragment() {

    private var visibleConnections = mutableListOf<String>()
    private val listAdapter = ConnectionsListAdapter { item -> onItemClick(item) }
    private var connecteeId: String = ""


    //Callback that handles discovery of advertising endpoints
    private val endpointDiscoveryCallback = object : EndpointDiscoveryCallback() {
        override fun onEndpointFound(endpointId: String, info: DiscoveredEndpointInfo) {
            Log.i("tag", "Endpoint found")
            visibleConnections.add(0, endpointId + '%' + info.endpointName)
            listAdapter.notifyItemInserted(0)
        }

        override fun onEndpointLost(endpointId: String) {
            val removeIndex = visibleConnections.indexOf(endpointId)
            if (removeIndex >= 0) {
                visibleConnections.removeAt(removeIndex)
                listAdapter.notifyItemRemoved(removeIndex)
            }
            Log.i("tag", "Endpoint lost")
        }

    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.join_session, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handlePermissions()

        connectionsRecycler.adapter = listAdapter
        connectionsRecycler.layoutManager = LinearLayoutManager(context)
        listAdapter.submitList(visibleConnections)

        startDiscovery()
    }

    //Choose an endpoint
    private fun onItemClick(connectionData: String) {
        val endpointId = connectionData.split('%')[0]
        if (joinName.text.isNullOrBlank()) {
            Toast.makeText(requireContext(), "Please enter a name!", Toast.LENGTH_LONG).show()
        } else {
            findNavController().navigate(JoinSessionFragmentDirections.navigateToCharactersSelection(endpointId, joinName.text.toString()))
        }
    }

    //Initiate discovery of advertising endpoints
    private fun startDiscovery() {
        Nearby.getConnectionsClient(requireContext()).startDiscovery("package", endpointDiscoveryCallback, DiscoveryOptions.Builder().setStrategy(
                Strategy.P2P_STAR).build()).addOnSuccessListener {
            println(connecteeId)
        }
    }

    //Check for required permissions and handle
    private fun handlePermissions() {
        val requestPermissionLauncher =
                registerForActivityResult(
                        ActivityResultContracts.RequestPermission()
                ) { isGranted: Boolean ->
                    if (isGranted) {
                        println("Permission is granted")
                    } else {
                        println("Permission is not granted")
                        Toast.makeText(context, "App functionality limited!", Toast.LENGTH_LONG).show()
                    }
                }
        //Launch permissions handler
        when {
            ContextCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED -> {
                println("Can use api")
                startDiscovery()
            }
            shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) -> {
                println("Cannot use api")
                Toast.makeText(context, "App functionality limited!", Toast.LENGTH_LONG).show()
            }
            else -> {
                println("Asking for permission")
                requestPermissionLauncher.launch(
                        Manifest.permission.ACCESS_FINE_LOCATION
                )
            }
        }
    }
}