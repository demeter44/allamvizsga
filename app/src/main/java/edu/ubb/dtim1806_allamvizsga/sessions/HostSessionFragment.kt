package edu.ubb.dtim1806_allamvizsga.sessions

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.nearby.Nearby
import com.google.android.gms.nearby.connection.*
import edu.ubb.dtim1806_allamvizsga.R
import kotlinx.android.synthetic.main.host_session.*
import java.io.File
import java.util.*


class HostSessionFragment : Fragment() {

    private var tempString: String = ""
    private var connecteeName: String = ""
    private val codeName = getDeviceName()
    private val playerList: MutableList<String> = mutableListOf()
    private val idToPlayer: MutableMap<String, String> = mutableMapOf()
    private val playerNames: MutableList<String> = mutableListOf()
    private val listAdapter = ConnectionsListAdapter { item -> onItemClick(item) }

    private fun onItemClick(item: String) {
        println(item)
    }

    //Callback that handles payload receivals
    private val payloadCallback = object : PayloadCallback() {
        override fun onPayloadReceived(endpointId: String, payload: Payload) {
            tempString = String(payload.asBytes()!!)
            Log.i("tag", "payload received")
            Log.i("tag", "payload is $tempString")
            when (tempString.split(" ")[0]) {
                "GETPLAYERS" -> {
                    println("received request to send playerlist")
                    println("sending payload to send players")
                    var s = "SENDPLAYERS "
                    for ((id, name) in idToPlayer) {
                        s += "$id%$name "
                    }
                    val bytesPayload = Payload.fromBytes(s.toByteArray(Charsets.UTF_8))
                    Nearby.getConnectionsClient(requireContext()).sendPayload(endpointId, bytesPayload)
                }
                "SENDITEM" -> {
                    println("received request to forward item")
                    println("sending payload to forward item")
                    val data = tempString.split(" ")
                    val itemName = data[1]
                    val itemDescription = data[2]
                    val itemQuantity = data[3]
                    val sendToId = data[4]
                    val bytesPayload = Payload.fromBytes("FORWARDITEM $itemName $itemDescription $itemQuantity $endpointId".toByteArray(Charsets.UTF_8))
                    Nearby.getConnectionsClient(requireContext()).sendPayload(sendToId, bytesPayload)
                    consoleWrite("${idToPlayer[endpointId]} : Sent item $itemName x$itemQuantity to player ${idToPlayer[sendToId]}")
                }

                else -> {
                    consoleWrite("${idToPlayer[endpointId]} : $tempString")
                }


            }

        }

        override fun onPayloadTransferUpdate(endpointId: String, update: PayloadTransferUpdate) {
            Log.i("tag", "Payload transfer update")
        }

    }

    //Connections handler callback
    private val connectionLifecycleCallback = object : ConnectionLifecycleCallback() {
        override fun onConnectionInitiated(endpointId: String, connectionInfo: ConnectionInfo) {
            Log.i("tag", "Accepting connection")
            connecteeName = connectionInfo.endpointName
            Nearby.getConnectionsClient(requireContext()).acceptConnection(endpointId, payloadCallback)
        }

        override fun onConnectionResult(endpointId: String, result: ConnectionResolution) {
            if (result.status.isSuccess) {
                Log.i("tag", "Connection successful")
                consoleWrite("Player $connecteeName joined!")
                playerList.add(endpointId)
                playerNames.add(0, connecteeName)
                listAdapter.notifyItemInserted(0)
                idToPlayer[endpointId] = connecteeName
            } else {
                Log.i("tag", "Connection failed")
            }
        }

        override fun onDisconnected(endpointId: String) {
            Log.i("tag", "Disconnected")
            consoleWrite("Player ${idToPlayer[endpointId]} left!")
            playerList.remove(endpointId)
            val removeIndex = playerNames.indexOf(idToPlayer[endpointId])
            playerNames.removeAt(removeIndex)
            listAdapter.notifyItemRemoved(removeIndex)
            idToPlayer.remove(endpointId)
        }

    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.host_session, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handlePermissions()
        Log.i("tag", "here we are")
        consoleView.text = "Start of event console \n"

        connectedPlayersRecycler.adapter = listAdapter
        connectedPlayersRecycler.layoutManager = LinearLayoutManager(context)
        listAdapter.submitList(playerNames)
        consoleView.movementMethod = ScrollingMovementMethod()

        //Clears the console
        clearLogBut.setOnClickListener {
            consoleView.text = "Cleared\n"
            Toast.makeText(context, "Console cleared!", Toast.LENGTH_SHORT).show()
        }

        saveLogBut.setOnClickListener {
            saveConsole()
        }

        startAdvertising()

    }

    //Saves the console as a .log file on the local file system
    private fun saveConsole() {
        val logsDir = requireContext().filesDir.path + "/logs"
        val logs = File(logsDir)
        logs.mkdir()

        val fileName = "RPSS${Calendar.getInstance().time}.log"
        val file = File(requireContext().filesDir.path + "/logs", fileName)
        file.writeText(consoleView.text.toString())
        file.createNewFile()
        Toast.makeText(context, "Saved console log!", Toast.LENGTH_SHORT).show()
    }

    //Writes to the console
    fun consoleWrite(s: String) {
        val consoleText = consoleView.text.toString() + s + '\n'
        consoleView.text = consoleText
    }

    //On fragment shutdown we make sure to stop all communication
    override fun onDestroy() {
        println("Destroy happened")
        Nearby.getConnectionsClient(requireContext()).stopAdvertising()
        Nearby.getConnectionsClient(requireContext()).stopAllEndpoints()
        super.onDestroy()
    }


    //Begin advertising as a host
    private fun startAdvertising() {
        Nearby.getConnectionsClient(requireContext()).startAdvertising(
                codeName, "package", connectionLifecycleCallback,
                AdvertisingOptions.Builder().setStrategy(Strategy.P2P_STAR).build()
        )
    }

    //Check for required permissions and handle
    private fun handlePermissions() {
        val requestPermissionLauncher =
                registerForActivityResult(
                        ActivityResultContracts.RequestPermission()
                ) { isGranted: Boolean ->
                    if (isGranted) {
                        println("Permission is granted")
                    } else {
                        println("Permission is not granted")
                        Toast.makeText(context, "App functionality limited!", Toast.LENGTH_LONG).show()
                    }
                }
        //Launch permissions handler
        when {
            ContextCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED -> {
                println("Can use api")
            }
            shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) -> {
                println("Cannot use api")
                Toast.makeText(context, "App functionality limited!", Toast.LENGTH_LONG).show()
            }
            else -> {
                println("Asking for permission")
                requestPermissionLauncher.launch(
                        Manifest.permission.ACCESS_FINE_LOCATION
                )
            }
        }
    }

    //Returns the current device model and manufacturer
    private fun getDeviceName(): String {
        val manufacturer = Build.MANUFACTURER
        val model = Build.MODEL
        return if (model.toLowerCase(Locale.ROOT).startsWith(manufacturer.toLowerCase(Locale.ROOT))) {
            capitalize(model)
        } else {
            capitalize(manufacturer) + " " + model
        }
    }


    //Returns capitalized string
    private fun capitalize(s: String?): String {
        if (s == null || s.isEmpty()) {
            return ""
        }
        val first = s[0]
        return if (Character.isUpperCase(first)) {
            s
        } else {
            Character.toUpperCase(first).toString() + s.substring(1)
        }
    }
}