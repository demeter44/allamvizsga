package edu.ubb.dtim1806_allamvizsga.mainmenu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import edu.ubb.dtim1806_allamvizsga.R
import kotlinx.android.synthetic.main.main_menu.*

class MainMenuFragment : Fragment() {
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.main_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Navigation
        templatesBut.setOnClickListener { findNavController().navigate(MainMenuFragmentDirections.navigateToTemplates()) }
        charactersBut.setOnClickListener { findNavController().navigate(MainMenuFragmentDirections.navigateToCharacters()) }
        hostBut.setOnClickListener { findNavController().navigate(MainMenuFragmentDirections.navigateToHostsession()) }
        joinBut.setOnClickListener { findNavController().navigate(MainMenuFragmentDirections.navigateToJoinsession()) }
    }

}