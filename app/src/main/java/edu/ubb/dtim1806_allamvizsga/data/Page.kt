package edu.ubb.dtim1806_allamvizsga.data

import java.io.Serializable

data class Page(
        val name: String,
        val intFields: List<IntegerField>? = null,
        val stringFields: List<StringField>? = null
) : Serializable
