package edu.ubb.dtim1806_allamvizsga.data

import java.io.Serializable

data class Template(
        val name: String,
        val author: String,
        val pages: List<Page> = emptyList()
) : Serializable