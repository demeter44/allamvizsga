package edu.ubb.dtim1806_allamvizsga.data

import java.io.Serializable

data class CharacterPage(
        val name: String,
        val template: Template,
        val inventory: MutableList<CharacterItem>
) : Serializable