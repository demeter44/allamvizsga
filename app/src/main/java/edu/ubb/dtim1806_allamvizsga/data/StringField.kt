package edu.ubb.dtim1806_allamvizsga.data

import java.io.Serializable

data class StringField(
        val name: String,
        var value: String
) : Serializable
