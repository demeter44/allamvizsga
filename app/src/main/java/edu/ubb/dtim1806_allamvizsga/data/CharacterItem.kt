package edu.ubb.dtim1806_allamvizsga.data

import java.io.Serializable

data class CharacterItem(
        val name: String,
        val description: String,
        val quantity: Int
) : Serializable
