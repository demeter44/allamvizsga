package edu.ubb.dtim1806_allamvizsga.data

import java.io.Serializable

data class IntegerField(
        val name: String,
        var value: Int,
        var maxValue: Int? = null,
        var modifier: Int? = null
) : Serializable